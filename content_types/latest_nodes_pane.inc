<?php
/**
 * @file
 */

/**
 * Callback function to supply a list of content types.
 */
function latest_nodes_latest_nodes_pane_ctools_content_types() {
  return array(
    'title' => t('Latest nodes'),
    'description' => t('View the latest nodes.'),
    'defaults' => array(
      'content_type' => '',
    ),
    'icon' => 'icon_block_custom.png',
    'single' => TRUE,
    'content_types' => array('latest_nodes_pane'),
    'render callback' => 'latest_nodes_pane_render',
    'edit form' => 'latest_nodes_pane_edit_form',
    'category' => t('Miscellaneous'),
  );
}

/**
 * Run-time rendering of the panels latest nodes pane.
 */
function latest_nodes_pane_render($subtype, $conf, $args, $context) {

  $nodes = latest_nodes_get_nodes($conf);
  if (is_array($nodes)) {

    // TODO: delta!
    $block = new stdClass();
    $block->module = 'latest_nodes';
    $block->delta = 0;
    $block->content = theme('latest_nodes_pane', $conf, $nodes);
    $block->title = '';
    // check override title
    if ($conf['override_title']) {
      $block->title = $conf['override_title_text'];
    }

    return $block;
  }
  else {
    return NULL;
  }

}

/**
 * 'Edit form' callback for the panels latest nodes pane type.
 */
function latest_nodes_pane_edit_form(&$form, &$form_state) {
  $types = array_keys(node_get_types('types'));
  $options = array();
  foreach ($types as $type) {
    $options[$type] = t($type);
  }

  $form['content_types'] = array(
    '#type' => 'select',
    '#multiple' => TRUE,
    '#default_value' => $form_state['conf']['content_types'] ? $form_state['conf']['content_types'] : '',
    '#options' => $options,
    '#size' => min(6, count($types)),
  );

  $form['display_type'] = array(
    '#type' => 'select',
    '#default_value' => $form_state['conf']['display_type'] ? $form_state['conf']['display_type'] : 'teaser',
    '#options' => array(
      'list' => 'list',
      'teaser' => 'teaser',
      'full_node' => 'full_node'
    ),
  );

  $form['offset'] = array(
    '#type' => 'textfield',
    '#default_value' => $form_state['conf']['offset'] ? $form_state['conf']['offset'] : 0,
  );

  $form['limit'] = array(
    '#type' => 'textfield',
    '#default_value' => $form_state['conf']['limit'] ? $form_state['conf']['limit'] : 10,
  );

  $form['cache_option'] = array(
    '#title' => 'cache settings',
    '#type' => 'select',
    '#default_value' => $form_state['conf']['cache_option'] ? $form_state['conf']['cache_option'] : 'none',
    '#options' => array(
      '0' => t('No cache'),
      '60' => '1 min',
      '300' => '5 min',
      '1800' => '30 min',
      '3600' => '1 hour',
      '86400' => '1 day',
      '604800' => '1 week',
    ),
    '#description' => t('Enable internal cache for this pane and set TTL. Cache will automatically be flushed on node insert.')
  );

  // TODO - save id not string
  return $form;
}

function latest_nodes_pane_edit_form_submit(&$form, &$form_state) {
  foreach (array('content_types', 'display_type', 'offset', 'limit', 'cache_option') as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

/**
 * Callback to provide the administrative title of the latest nodes pane.
 */
function latest_nodes_latest_nodes_pane_content_type_admin_title($subtype, $conf) {
  $output = t('Latest nodes view');
  return $output;
}

/**
 * Callback to provide administrative info for the latest nodes pane.
 */
function latest_nodes_latest_nodes_pane_content_type_admin_info($subtype, $conf) {
  $block = new stdClass();

  if ($conf['override_title']) {
    // Don't translate this one
    $block->title = $conf['override_title_text'] . ' (' . $conf['display_type'] . ')';
  }
  else {
    $block->title = t('Latest nodes (@display_type)', array('@display_type' => $conf['display_type']));
  }

  return $block;
}

